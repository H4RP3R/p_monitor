#! -*- coding: utf-8 -*-

import os, wmi, time
import colorama as clr
from email.mime.text import MIMEText
import smtplib
import psutil


clr.init()
SLEEPTIME = 60
f = open('hostname.txt', 'r')
HOSTNAME = f.read()
f = open('emails.txt', 'r')
addressees = f.read()
addressees = [a.strip() for a in addressees.split()]


def getProcessToStart():
	'''Get process names from bat files'''
	startProcessList = []
	paths = ('runall.bat', r'C:\Parser\run\runall.bat', r'C:\Parser\run\RUNALL.bat') #Directories 
											#where the script searches for runall file


	f = None
	if not f:
		for path in paths:
			try:
				f = open(path, 'r')
				batPath = path[: -10]
			except:
				print('searching runall.bat')

	if not f:
		print('ERROR runall.bat not found')

	batFiles = f.read()
	batList = [b.split(' ')[-1] for b in batFiles.split('\n') if b.split(' ')[0]=='start']

	for b in batList:
		try:
			f = open(batPath+b, 'r')
			fileText = f.read()
			for x in fileText.split():
				if '.exe' in x:
					startProcessList.append(x.lower())
			f.close()
		except FileNotFoundError:
			pass

	return tuple(startProcessList)


def checkProcesses(startProcessList):
	runningProcesses = [x.name() for x in psutil.process_iter()]

	proc = []
	while runningProcesses:
		string = runningProcesses.pop()
		proc.append(string.lower())

	if 'werfault.exe' in proc:
		allert = True
	else:
		allert = False
	
	for p in proc:
		if p in startProcessList:
			startProcessList.remove(p)

	return startProcessList, allert


def sendMessage(HOSTNAME, addressees,messageTrigerOn, messageWasSent):
	# SMTP-server
	server = 'smtp.gmail.com'
	port = 587 #25
	password = 'сюда писать пароль от почты'

	message = HOSTNAME+'\nЕсть нерабочие процессы'

	sender = 'messenger2525@gmail.com'
	subject = 'Processes alert: ' + str(HOSTNAME)

	try:
		for addressee in addressees:
			# create message
			msg = MIMEText(message, '', 'utf-8')
			msg['Subject'] = subject
			msg['From'] = sender
			msg['To'] = addressee

			# send message
			s = smtplib.SMTP(server, port)
			s.ehlo()
			s.starttls()
			s.ehlo()
			s.login(sender, password)
			s.sendmail(sender, addressee, msg.as_string())
			s.quit()
			print('send message to '+addressee)

		messageTrigerOn  = False
		messageWasSent = True
	except:
		print("can't send message")

	return messageTrigerOn, messageWasSent


def printOutput(fallenProcesses, allert):
	if len(fallenProcesses) > 0 or allert:
		print(clr.Fore.RED+'process fell!!!'+clr.Style.RESET_ALL)
		messageTrigerOn = True
	else:
		print(clr.Fore.GREEN+'All processes work'+clr.Style.RESET_ALL)
		messageTrigerOn = False

	return messageTrigerOn


def main():

	startProcessList = getProcessToStart()
	messageWasSent = False

	while True:
		print('sleep '+str(SLEEPTIME)+' sec')
		time.sleep(SLEEPTIME)

		if len(startProcessList) > 0:
			fallenProcesses, allert = checkProcesses(list(startProcessList[ : ]))
			messageTrigerOn = printOutput(fallenProcesses, allert)
			if messageTrigerOn and not messageWasSent:
				messageTrigerOn, messageWasSent = sendMessage(HOSTNAME,
				addressees, messageTrigerOn, messageWasSent)
		else:
			print("there's no processes to run")
			break


if __name__ == '__main__':
	main()
