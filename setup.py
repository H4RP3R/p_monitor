import sys
from cx_Freeze import setup, Executable
import os


# Dependencies are automatically detected, but it might need fine tuning.
build_exe_options = {"excludes": ["tkinter"]}

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None

setup(name = 'p_monitor',
	version = '0.2',
	description = '',
	options = {'build_exe': build_exe_options},
	executables = [Executable('p_monitor.py', base=base)])
